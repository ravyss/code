package collections.collectionsTask_05;

import collections.collectionsTask_01.Person;

public interface PersonService {
	
	boolean addPerson(Person person);
	
	Person getPersonById(long id);
	
	boolean removePersonById(long id);
	
	boolean changeAge(long id, short age);
	
	boolean changeName(long id, String name);
	
	boolean changeSurname(long id, String surname);

}
