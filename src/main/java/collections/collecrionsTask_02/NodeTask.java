package collections.collecrionsTask_02;

import java.util.ArrayList;
import java.util.List;

public class NodeTask {
	public static void main(String[] args) {

		// itialize Nodes with previous nodes
		Node<String> node1 = new Node<String>(null, "Vitalik", null);
		Node<String> node2 = new Node<String>(null, "Anton", node1);
		Node<String> node3 = new Node<String>(null, "Artyom", node2);
		Node<String> node4 = new Node<String>(null, "Alex", node3);
		Node<String> node5 = new Node<String>(null, "Lara", node4);
		Node<String> node6 = new Node<String>(null, "Vitalik", node5);
		Node<String> node7 = new Node<String>(null, "Karl", node6);

		// Initialize Nodes with newxt nodes
		initNextNode(node7);
		
		Node<String> lastNode = node1;	
		while(lastNode.getNextNode()!=null) {
			lastNode = lastNode.getNextNode();
		}
		Node<String> header = new Node<String>(node1, null, lastNode);	
		
		int numberOfSwitchble = 3;

		NodeSwitcher switcher = new NodeSwitcher();
		switcher.switchFirstAndLastElem(numberOfSwitchble, header);
		
		
		
//		while (frontNode.getNextNode() != null & !frontNode.equals(backNode)) {
//			for (int i = 0; i < n; i++) {
//				frontNode = frontNode.getNextNode();
//			}
//			for (int i = 0; i < n; i++) {
//				backNode = backNode.getPreviousNode();
//			}
//			
//			Node<String> switchFrontNode = frontNode.getPreviousNode();
//			Node<String> switchBackNode = backNode.getNextNode();
//			Node<String> previousFront = 
//			if(previousFront==backNode)break;
//			frontNode.setPreviousNode(switchBackNode);
//			backNode.setNextNode(switchFrontNode);  			
//			
//		}
		
		NodeReversing reverse = new NodeReversing();
		reverse.reverse(node1);

	}

	/*
	 * take the last node
	 */
	public static void initNextNode(Node<String> nextNode) {
		Node<String> loopNode = nextNode;
		while (loopNode.getPreviousNode() != null) {
			loopNode.getPreviousNode().setNextNode(loopNode);
			loopNode = loopNode.getPreviousNode();
		}
	}
}
