package collections.collectionsTask_01;

import java.util.Comparator;

public class sortedBySurname implements Comparator<Person>{

	@Override
	public int compare(Person o1, Person o2) {
      String str1 = o1.getSurname();
      String str2 = o2.getSurname();
          
      return str2.compareTo(str1);
	}
}
