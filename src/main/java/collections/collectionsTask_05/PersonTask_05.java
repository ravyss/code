package collections.collectionsTask_05;

import java.util.ArrayList;
import java.util.List;

import collections.collectionsTask_01.Person;

public class PersonTask_05 {
	public static void main(String[] args) {
		List<Person> students = new ArrayList<>();
		int numberOFPersons = 1000;
		
		for(int i = 0; i<numberOFPersons; i++) {
			students.add(new Person(i));
		}
		
		PersonCache cache = new PersonCache();
		PersonService personService = new PersonServiceImpl(cache);
		PersonMgr studentsMeneger = new PersonMgr(personService);
		PersonChangeListener listener = new PersonChangeListenerImpl(studentsMeneger);
		PersonChangeListener removeListener = new PersonRemoveListener(studentsMeneger);
		
		for (Person person : students) {
			studentsMeneger.addPerson(person);
		}
		
		studentsMeneger.removePersonById(30L);
		studentsMeneger.removePersonById(50L);
		
		System.out.println( students.get(0).getFirstName());		
		System.out.println( studentsMeneger.changeName(0, "Igor"));
		System.out.println( personService.getPersonById(0).getFirstName());
	}

}
