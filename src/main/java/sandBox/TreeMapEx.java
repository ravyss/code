package sandBox;

import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class TreeMapEx {
	public static void main(String[] args) {
		AddMap key1 = new AddMap("1");
		AddMap key2 = new AddMap("2");
		AddMap key3 = new AddMap("3");

		Map<AddMap, String> map = new TreeMap<>(new Comparator<AddMap>() {

			@Override
			public int compare(AddMap o1, AddMap o2) {
				String value1 = o1.getValue();
				String value2 = o2.getValue();

				return value2.equals(null) ? value1.equals(null) ? 0 : value2.compareTo(value1)
						: value1.compareTo(value2);
			}

		}
//				(compare1,compare2) ->{
//			String value1 = compare1.getValue();
//			String value2 = compare2.getValue();
//			
//			return value2.equals(null) ? 
//					value1.equals(null) ? 0 : value2.compareTo(value1) 
//							: value1.compareTo(value2);
//		}
		);
		map.put(key3, "string3");
		map.put(key1, "string1");
		map.put(key2, "string2");

		Set<Entry<AddMap, String>> set = map.entrySet();

		set.forEach(entry -> {
			System.out.println(entry.getKey() + " " + entry.getValue());
		});

	}
}

class AddMap implements Comparable<AddMap> {
	private String value;

	/**
	 * @param value
	 */
	public AddMap(String value) {
		super();
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddMap other = (AddMap) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {

		return value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public int compareTo(AddMap o) {
		String value1 = o.getValue();
		String value2 = value;

		return value2.equals(null) ? value1.equals(null) ? 0 : value2.compareTo(value1) : value1.compareTo(value2);
	}

}
