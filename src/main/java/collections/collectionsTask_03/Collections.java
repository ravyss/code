package collections.collectionsTask_03;

import java.util.ArrayDeque;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Queue;

public class Collections {
	public static void main(String[] args) {
		List<String> list = List.of("one", "two", "three", "four","five","six","seven","eight");
		LinkedHashSet<String> set = new LinkedHashSet<>();
		Queue<String> queue = new ArrayDeque<>();
		set.addAll(list);
		queue.addAll(list);

		ReversiveIterator<String> reversList = new ReversiveIterator<>(list);
		ReversiveIterator<String> reversSet = new ReversiveIterator<>(set);
		ReversiveIterator<String> reversQueue = new ReversiveIterator<>(queue);

		while(reversList.hasNext()) {
			System.out.println(reversList.next());
		}

	}
}
