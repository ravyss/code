package collections.collectionsTask_05;

import collections.collectionsTask_01.Person;

public class PersonException extends Exception{
	
	public PersonException(Throwable couse, Person person) {
		super("Wrong parameters: " + "id-" + person.getId() + ", age-" +  person.getAge() + ", name:" + person.getFirstName() + ", surname:" + person.getSurname(), couse);	
		
	}
	
	public PersonException(Person person) {
		super("Wrong parameters: " + "id:" + person.getId() + ", age:" +  person.getAge() + ", name:" + person.getFirstName() + ", surname:" + person.getSurname());
	}
	
}
