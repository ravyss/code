package sandBox;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class IteratorPractice {
	public static void main(String[] args) {
		Node<String> node7 = new Node<String>(null, "Karl");
		Node<String> node6 = new Node<String>(node7, "Jack");
		Node<String> node5 = new Node<String>(node6, "Anton");
		Node<String> node4 = new Node<String>(node5, "Artyom");
		Node<String> node3 = new Node<String>(node4, "Alex");
		Node<String> node2 = new Node<String>(node3, "Lara");
		Node<String> node1 = new Node<String>(node2, "Vitalik");

		int n = 3;

		Node<String> firstPointerNode = node1;
		Node<String> secondPointerNode = node1;
		while (firstPointerNode.getNextNode() != null) {
			firstPointerNode = firstPointerNode.getNextNode();

			if (n != 0)
				n--;
			if (n == 0) {
				secondPointerNode = secondPointerNode.getNextNode();
			}

			if (firstPointerNode.getNextNode() == null) {
				System.out.println(n == 0 ? secondPointerNode.getElem() : "not found!");
			}

		}

//		LinkedList<String> names = new LinkedList<>();
//		names.add("Vitalik");
//		names.add("Lara");
//		names.add("Alex");
//		names.add("Artyom");
//		names.add("Anton");
//		names.add("Jack");
//		names.add("Karl");
//
//		Iterator<String> iterator = names.iterator();
//		ListIterator<String> listIterator = names.listIterator();

//		for (int i = 1; listIterator.hasNext(); i++) {
//			String a = listIterator.next();
//			if (i == n) {
//				System.out.println(a);
//			}
//		}
//		
//		for (int i = 1; listIterator.hasPrevious(); i++) {
//			String a = listIterator.previous();
//			if (i == n) {
//				System.out.println(a);
//			}
//		}

//
//		int i = 1;
//		while (listIterator.hasNext()) {			
//			String a = listIterator.next();
//			if (i == n) {
//				System.out.println(a);
//			}
//			i++;
//		}
//		
//		int i1 = 1;
//		while (listIterator.hasPrevious()) {			
//			String a = listIterator.previous();
//			if (i1 == n) {
//				System.out.println(a);
//			}
//			i1++;
//		}

//		while (listIterator.hasNext()) {			
//			String a = listIterator.next();
//			System.out.println(a);
//		}

//		while (listIterator.hasPrevious()) {
//			String a = listIterator.previous();
//			if (a.equals("Lara")) {
//				listIterator.remove();
//				continue;
//			}
//			System.out.println(a);
//		}

//		while(iterator.hasNext()) {
//			String a = iterator.next();
//			System.out.println(a);
//		}

	}
}
