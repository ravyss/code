package collections.collectionsTask_05;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import collections.collectionsTask_01.Person;

public class PersonServiceNonMockTest {
	private PersonServiceImpl personService;
	static final long ID = 0;
	
	@Test
	public void ChangeAge_CorrectAge_True() {
		//given
		short age = 22;
		Person personToSave = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		//when
		personService.addPerson(personToSave);
		boolean isChanged = personService.changeAge(ID, age);
		Person personToGet = personService.getPersonById(ID);
		
		//then	
		assertTrue(isChanged);		
		assertSame(age, personToGet.getAge());	
		
	}
	
	
	@Test
	public void ChangeAge_SameAge_False() {
		//given
		short age = 47;
		Person personToSave = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		//when
		personService.addPerson(personToSave);
		boolean isChanged = personService.changeAge(ID, age);
		Person personToGet = personService.getPersonById(ID);
		
		//then
		assertFalse(isChanged);		
		assertSame(personToSave.getAge(), personToGet.getAge());	
	}
	
	@Test
	public void ChangeAge_UncorrectAge_False() {
		//given
		short age = -1;
		Person personToSave = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		//when
		personService.addPerson(personToSave);
		boolean isChanged = personService.changeAge(ID, age);
		Person personToGet = personService.getPersonById(ID);
		
		//then
		assertFalse(isChanged);
		
	}
	
	@Test
	public void ChangeName_CorrectName_True() {
		//given
		String name = "test";
		Person personToSave = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		//when
		personService.addPerson(personToSave);
		boolean isChanged = personService.changeName(ID, name);
		Person personToGet = personService.getPersonById(ID);
		
		//then	
		assertTrue(isChanged);		
		assertSame(name, personToGet.getFirstName());	
		
	}
	
	@Test
	public void ChangeName_NameAreSame_False() {
		//given
		String name = "NAme";
		Person personToSave = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		//when
		personService.addPerson(personToSave);
		boolean isChanged = personService.changeName(ID, name);
		Person personToGet = personService.getPersonById(ID);
		
		//then	
		assertFalse(isChanged);		
		assertSame(personToSave.getFirstName(), personToGet.getFirstName());	
	}
	
	@Test
	public void ChangeName_NameAreNull_False() {
		//given
		String name = null;
		Person personToSave = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		//when
		personService.addPerson(personToSave);
		boolean isChanged = personService.changeName(ID, name);
		Person personToGet = personService.getPersonById(ID);
		
		//then	
		assertFalse(isChanged);	
	}
	
	
	@Test
	public void ChangeSurame_CorrectSurname_True() {
		//given
		String surname = "test";
		Person personToSave = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		//when
		personService.addPerson(personToSave);
		boolean isChanged = personService.changeSurname(ID, surname);
		Person personToGet = personService.getPersonById(ID);
		
		//then	
		assertTrue(isChanged);		
		assertSame(surname, personToGet.getSurname());	
	}
	
	@Test
	public void ChangeSurname_SurnameAreSame_False() {
		//given
		String surname = "surname";
		Person personToSave = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		//when
		personService.addPerson(personToSave);
		boolean isChanged = personService.changeSurname(ID, surname);
		Person personToGet = personService.getPersonById(ID);
		
		//then	
		assertFalse(isChanged);		
		assertSame(personToSave.getSurname(), personToGet.getSurname());	
	}
	
	@Test
	public void ChangeSurname_SurnameAreNull_False() {
		//given
		String surname = null;
		Person personToSave = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		//when
		personService.addPerson(personToSave);
		boolean isChanged = personService.changeSurname(ID, surname);
		Person personToGet = personService.getPersonById(ID);
		
		//then	
		assertFalse(isChanged);		

	}
	
	@Before
	public void before() {
		personService = new PersonServiceImpl(new PersonCache());
	}
	
	@After
	public void after() {
		personService = null;
	}

}
