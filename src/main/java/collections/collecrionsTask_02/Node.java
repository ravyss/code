package collections.collecrionsTask_02;

public class Node<T> {
	private Node<T> previousNode;
	private T element;
	private Node<T> nextNode;
	
	
	
	/**
	 * @param previousNode
	 * @param element
	 * @param nextNode
	 */
	public Node(Node<T> nextNode, T element, Node<T> previousNode) {
		super();
		this.previousNode = previousNode;
		this.element = element;
		this.nextNode = nextNode;
	}
	
	public Node<T> getPreviousNode() {
		return previousNode;
	}
	public void setPreviousNode(Node<T> previousNode) {
		this.previousNode = previousNode;
	}
	public T getElement() {
		return element;
	}
	public void setElement(T element) {
		this.element = element;
	}
	public Node<T> getNextNode() {
		return nextNode;
	}
	public void setNextNode(Node<T> nextNode) {
		this.nextNode = nextNode;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((element == null) ? 0 : element.hashCode());
		result = prime * result + ((nextNode == null) ? 0 : nextNode.hashCode());
		result = prime * result + ((previousNode == null) ? 0 : previousNode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (element == null) {
			if (other.element != null)
				return false;
		} else if (!element.equals(other.element))
			return false;
		if (nextNode == null) {
			if (other.nextNode != null)
				return false;
		} else if (!nextNode.equals(other.nextNode))
			return false;
		if (previousNode == null) {
			if (other.previousNode != null)
				return false;
		} else if (!previousNode.equals(other.previousNode))
			return false;
		return true;
	}
	
	
}
