package parserTask;

public class likeJsonFile {
	public String jsonString(String[][] toJson) {

		String jsonFile = "{";
		for (int i = 0; i < toJson[0].length; i++) {
			jsonFile = jsonFile + "\"" + toJson[0][i] + "\": ";
			if (i < toJson[0].length - 1)
				jsonFile = jsonFile + toJson[1][i] + ",";
			else if (i == toJson[0].length - 1)
				jsonFile = jsonFile + toJson[1][i];
		}
		jsonFile = jsonFile + "}";
		return jsonFile;
	}
}
