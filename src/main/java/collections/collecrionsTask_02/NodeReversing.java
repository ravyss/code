package collections.collecrionsTask_02;

public class NodeReversing {
	/**
	 * take the first node
	 * @param node
	 */
	public void reverse(Node<String> node) {

		Node<String> loopNode = node;
				Node<String> switchNode = null;
				
				//reverse queue
				while(loopNode.getNextNode()!=null) {
					switchNode = new Node<String>(loopNode.getNextNode(), null, loopNode.getPreviousNode());
					loopNode.setNextNode(switchNode.getPreviousNode());
					loopNode.setPreviousNode(switchNode.getNextNode());
					
					loopNode = loopNode.getPreviousNode();
				}
	}

}
