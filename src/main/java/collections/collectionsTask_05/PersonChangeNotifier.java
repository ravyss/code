package collections.collectionsTask_05;
//subscriber
public interface PersonChangeNotifier {
	//notify
	boolean addListener(PersonChangeListener listener);
	
	boolean removeListener(PersonChangeListener listener);
	
	void fireChange(ChangeEvent change);

}
