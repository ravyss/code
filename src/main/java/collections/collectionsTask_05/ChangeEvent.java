package collections.collectionsTask_05;

import collections.collectionsTask_01.Person;

public class ChangeEvent {
	private Person beforeChangePerson;
	private Person afterChangePerson;
	private TypeChange type;
	
	/**
	 * @param beforeChangePerson
	 * @param afterChangePerson
	 */
	public ChangeEvent(Person beforeChangePerson, Person afterChangePerson, TypeChange type) {
		super();
		this.beforeChangePerson = beforeChangePerson;
		this.afterChangePerson = afterChangePerson;
		this.type = type;
		
	}
	
	public Person getBeforeChangePerson() {
		return beforeChangePerson;
	}
	
	public Person getAfterChangePerson() {
		return afterChangePerson;
	}
	

	public TypeChange getType() {
		return type;
	}
	
	
	public enum TypeChange{
		Add("added"),
		Remove("removed"),
		Update("updated");
		
		private String type;
		
		TypeChange(String type) {
			this.type = type;
		}
		
		public String getNameOFType() {
			return type;
		}
		
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((afterChangePerson == null) ? 0 : afterChangePerson.hashCode());
		result = prime * result + ((beforeChangePerson == null) ? 0 : beforeChangePerson.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChangeEvent other = (ChangeEvent) obj;
		if (afterChangePerson == null) {
			if (other.afterChangePerson != null)
				return false;
		} else if (!afterChangePerson.equals(other.afterChangePerson))
			return false;
		if (beforeChangePerson == null) {
			if (other.beforeChangePerson != null)
				return false;
		} else if (!beforeChangePerson.equals(other.beforeChangePerson))
			return false;
		if (type != other.type)
			return false;
		return true;
	}



}
