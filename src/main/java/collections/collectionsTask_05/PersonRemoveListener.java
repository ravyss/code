package collections.collectionsTask_05;

import collections.collectionsTask_05.ChangeEvent.TypeChange;

public class PersonRemoveListener implements PersonChangeListener {

	private PersonMgr mgr;

	/**
	 * @param mgr
	 */
	public PersonRemoveListener(PersonMgr meneger) {
		super();
		this.mgr = meneger;
		meneger.addListener(this);
	}

	@Override
	public void change(ChangeEvent change) {

		if (TypeChange.Remove == change.getType()) {
			TypeChange removedType = change.getType();
			System.out.println(
					"Person was " + removedType.getNameOFType() + " #" + change.getBeforeChangePerson().getId());
		}
	}

}
