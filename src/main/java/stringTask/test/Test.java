package stringTask.test;

public class Test {
	private String testString;

	public Test(String testString) {
		this.testString = testString;
	}

	/**
	 * return index of the consident char testString
	 * 
	 * @param profString
	 * @return
	 */
	public String oneCoincident(String profString) {
		String x = "";
		T: for (int i = 0; i < testString.length(); i++) {
			for (int j = 0; j < profString.length(); j++) {
				if (String.valueOf(testString.charAt(i)).equals(String.valueOf(profString.charAt(j)))) {
					x += i + " ";
					break T;
				}
			}
		}
		return x;
	}

	/**
	 * return all index of the consident char testString
	 * 
	 * @param profString
	 * @return
	 */
	public String allCoincident(String profString) {
		String x = "";
		for (int i = 0; i < testString.length(); i++) {
			for (int j = 0; j < profString.length(); j++) {
				if (String.valueOf(testString.charAt(i)).equals(String.valueOf(profString.charAt(j)))) {
					x += i + " ";
				}
			}
		}
		return x;
	}

}
