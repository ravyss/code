package stringTask;

import java.util.Arrays;


public class EditorString {
	private String editorStr;

	public EditorString(String editorStr) {// constructor of editorString
		this.editorStr = editorStr;

	}

	/**
	 * return index of the consident char testString
	 * 
	 * @param profString
	 * @return
	 */
	public String oneCoincident(String profString) {
		String x = "";
		T: for (int i = 0; i < editorStr.length(); i++) {
			for (int j = 0; j < profString.length(); j++) {
				if (String.valueOf(editorStr.charAt(i)).equals(String.valueOf(profString.charAt(j)))) {
					x += i + " ";
					break T;
				}
			}
		}
		System.out.println(x);
		return x;
	}

	/**
	 * return all index of the consident char testString
	 * 
	 * @param profString
	 * @return
	 */
	public String allCoincident(String profString) {
		String x = "";
		for (int i = 0; i < editorStr.length(); i++) {
			for (int j = 0; j < profString.length(); j++) {
				if (String.valueOf(editorStr.charAt(i)).equals(String.valueOf(profString.charAt(j)))) {
					x += i + " ";
				}
			}
		}
		System.out.println(x);
		return x;
	}

	/**
	 * deleted all spaces int string and sout how many they have
	 * 
	 */
	public int deleteReturnSpaces() {
		String spaceless = editorStr.replace(" ", "");
		System.out.println("Total spaces: " + (editorStr.length() - spaceless.length()));
		return editorStr.length() - spaceless.length();
	}

	/**
	 * sout number of Vowel and Consonant in string
	 * 
	 */
	public int[] numberOfVowelConsonant() {
		int[] count = new int[2];
		editorStr = editorStr.toLowerCase();

		for (int i = 0; i < editorStr.length(); i++) {
			if (editorStr.charAt(i) == 'a' || editorStr.charAt(i) == 'e' || editorStr.charAt(i) == 'i'
					|| editorStr.charAt(i) == 'o' || editorStr.charAt(i) == 'u') {
				count[0]++;
			} else if (editorStr.charAt(i) >= 'a' && editorStr.charAt(i) <= 'z') {
				count[1]++;
			}
		}
		System.out.println("Number of vowels: " + count[0]);
		System.out.println("Number of consonants: " + count[1]);
		return count;
	}

	/**
	 * sout which later was first in string
	 * 
	 */
	public String whoIsFirst() {
		editorStr = editorStr.toLowerCase();

		for (int i = 0; i < editorStr.length(); i++) {
			if (editorStr.charAt(i) == 'f') {
				System.out.println("\"j\" is lose, \"f\" win!");
				return "\"j\" is lose, \"f\" win!";
			} else if (editorStr.charAt(i) == 'j') {
				System.out.println("\"f\" is lose, \"j\" win!");
				return "\"f\" is lose, \"j\" win!";
			}
		}
		return "\"f\" and \"j\" not exist!";
	}

	/**
	 * sout prof of palindrome inputing string, if not create palindrome from
	 * charcters of the getting string
	 * 
	 * @param polindrome
	 * @return
	 */
	public String polindromeProf() {
		editorStr = editorStr.toLowerCase();
		String newbePalindrome = "";

		StringBuilder reversePolindrome = new StringBuilder();
		reversePolindrome.append(editorStr);
		reversePolindrome.reverse();

		if (editorStr.equals(reversePolindrome.toString())) {
			System.out.println("this is polindrome: " + editorStr);
		} else {
			editorStr = editorStr.replace(" ", "");

			char[] chars = editorStr.toCharArray();
			Arrays.sort(chars);
			String sorted = new String(chars);
			String sortedMod = String.valueOf(sorted);

			for (int i = 0; i < sortedMod.length() - 1; i++) {
				for (int j = i + 1; j < sortedMod.length(); j++) {
					if (String.valueOf(sortedMod.charAt(i)).equals(String.valueOf(sortedMod.charAt(j)))) {
						sortedMod = sortedMod.replaceFirst(String.valueOf(sortedMod.charAt(i)), "@");
					}
				}
			}

			String str = (sortedMod.substring(0, sortedMod.length())).replace("@", "");

			char[] arraySorted = str.toCharArray();
			int[] arrayNumeric = new int[str.length()];

			int numberOfChar;

			for (int i = 0; i < str.length(); i++) {
				numberOfChar = 0;
				for (int j = 0; j < sorted.length(); j++) {
					if (String.valueOf(str.charAt(i)).equals(String.valueOf(sorted.charAt(j)))) {
						numberOfChar++;
					}
					arrayNumeric[i] = numberOfChar;
				}
			}

			int numberOfOdd = 0;

			for (int i = 0; i < arrayNumeric.length; i++) {
				if (arrayNumeric[i] % 2 != 0) {
					numberOfOdd++;
				}
			}

			if (numberOfOdd > 1) {
				System.out.println("can't make palindrome from this characters!");
			} else if (numberOfOdd < 2) {
				String newPalindrome = "";

				for (int i = 0; i < str.length(); i++) {
					if (arrayNumeric[i] % 2 == 0) {
						newPalindrome = newPalindrome + String.valueOf(arraySorted[i]).repeat(arrayNumeric[i] / 2);
					} else if (arrayNumeric[i] % 2 != 0 && arrayNumeric[i] > 1) {
						newPalindrome = newPalindrome
								+ String.valueOf(arraySorted[i]).repeat((arrayNumeric[i] - 1) / 2);
					}
				}
				StringBuilder input1 = new StringBuilder();
				input1.append(newPalindrome).reverse();

				System.out.println("New palindrome is - " + newPalindrome + diffString(newPalindrome, str) + input1);
				newbePalindrome = newPalindrome + diffString(newPalindrome, str) + input1;

			}
		}
		return newbePalindrome;
	}
	/**
	 * return different betwen two string
	 * @param s1
	 * @param s2
	 * @return
	 */
	String diffString(String s1, String s2) {
		String odd = s2;
		for (int i = 0; i < s1.length(); i++) {
			for (int j = 0; j < s2.length(); j++) {
				if (String.valueOf(s1.charAt(i)).equals(String.valueOf(s2.charAt(j)))) {
					odd = odd.replace(String.valueOf(s1.charAt(i)), "@");
				}
			}
		}return odd.replace("@", "");
	}

	/**
	 * queue from vovel and consonant, and sout rest of the characters
	 * 
	 */
	public void queueVowelConsonant() {
		editorStr = editorStr.toLowerCase().replaceAll("\\s+", "");

		String vowel = "";
		String consonant = "";
		String qVC = "";

		for (int i = 0; i < editorStr.length(); i++) {
			if (editorStr.charAt(i) == 'a' || editorStr.charAt(i) == 'e' || editorStr.charAt(i) == 'i'
					|| editorStr.charAt(i) == 'o' || editorStr.charAt(i) == 'u') {
				vowel = vowel + editorStr.charAt(i);
			} else if (editorStr.charAt(i) >= 'a' && editorStr.charAt(i) <= 'z') {
				consonant = consonant + editorStr.charAt(i);
			}
		}
		if (vowel.length() == consonant.length()) {
			for (int i = 0; i < vowel.length(); i++) {
				qVC = qVC + vowel.charAt(i) + consonant.charAt(i);
			}
			System.out.println(qVC);
		} else if (vowel.length() > consonant.length()) {
			int difirent = vowel.length() - consonant.length();

			for (int i = 0; i < consonant.length(); i++) {
				qVC = qVC + vowel.charAt(i) + consonant.charAt(i);
			}
			System.out.println(qVC + " " + vowel.substring(difirent));
		} else if (consonant.length() > vowel.length()) {
			int difirent = consonant.length() - vowel.length();

			for (int i = 0; i < vowel.length(); i++) {
				qVC = qVC + vowel.charAt(i) + consonant.charAt(i);
			}
			System.out.println(
					"queue of Vowel and Consonant - \"" + qVC + "\" and rest chars - " + consonant.substring(difirent));
		}
	}

	/**
	 * multiply the single string
	 * 
	 * @return
	 * 
	 */
	public String multiplyString() {
		System.out.println("triple string - " + editorStr.repeat(3));
		return editorStr.repeat(3);
	}

	/**
	 * sout first half of the string
	 * 
	 */
	public String firstHalfString() {
		System.out.println("first half of the string - " + editorStr.substring(0, editorStr.length() / 2));
		return editorStr.substring(0, editorStr.length() / 2);
	}

	/**
	 * sout second half of the string
	 * 
	 */
	public String lastHalfString() {
		System.out.println("second half of the string - " + editorStr.substring(editorStr.length() / 2));
		return editorStr.substring(editorStr.length() / 2);
	}

}
