package parserTask;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class UserParser implements Parser<User> {

	@Override
	public User parse(String jsonResponse) {
		String name = "name";
		String screenName = "screen_name";
		String location = "location";
		String description = "description";
		String profileImageUrl = "profile_image_url";
		String id = "id";
		String followersCount = "followers_count";
		String favouritesCount = "favourites_count";

		FileReader reader;
		JSONParser parser = new JSONParser();
		JSONObject jsonObject;
		try {
			reader = new FileReader(jsonResponse);
			jsonObject = (JSONObject) parser.parse(reader);
			User info = new User((Long) jsonObject.get(id), (Long) jsonObject.get(followersCount),
					(Long) jsonObject.get(favouritesCount), (String) jsonObject.get(screenName),
					(String) jsonObject.get(location), (String) jsonObject.get(description),
					(String) jsonObject.get(profileImageUrl), (String) jsonObject.get(name));
			return info;
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
}
