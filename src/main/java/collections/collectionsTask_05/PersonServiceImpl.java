package collections.collectionsTask_05;

import collections.collectionsTask_01.Person;

public class PersonServiceImpl implements PersonService {

	private final PersonCache cache;

	PersonServiceImpl(PersonCache cache) {
		this.cache = cache;
	}

	@Override
	public boolean addPerson(Person person) {
		if (person == null)
			return false;

		return cache.addPerson(person);

	}

	@Override
	public Person getPersonById(long id) {
		return cache.getPersonById(id);
	}

	@Override
	public boolean removePersonById(long id) {
		return cache.removePersonById(id);
	}

	@Override
	public boolean changeAge(long id, short age) {
		
		Person person = cache.getPersonById(id);
		

		if(person == null) return false;
		if(person.equals(Person.NULL_PERSON)) return false;
		if(person.getAge() == age) return false;
		
		String name = person.getFirstName();
		String surname = person.getSurname();
		
		boolean isChanged = update(id, age, name, surname);
		return isChanged;
	}
	
	@Override
	public boolean changeName(long id, String name) {
		
		Person person = cache.getPersonById(id);

		if(person == null) return false;
		if(person.equals(Person.NULL_PERSON)) return false;
		if(person.getFirstName() == name) return false;
		
		short age = person.getAge();
		String surname = person.getSurname();
		
		boolean isChanged = update(id, age, name, surname);	
		return isChanged;
	}

	@Override
	public boolean changeSurname(long id, String surname) {
		
		Person person = cache.getPersonById(id);

		if(person == null) return false;
		if(person.equals(Person.NULL_PERSON)) return false;
		if(person.getSurname() == surname) return false;
		
		String name = person.getFirstName();
		short age = person.getAge();
		
		boolean isChanged = update(id, age, name, surname);	
		return isChanged;
	}
	
	
	private boolean update(Long id, short age, String name, String surname) {	
				
		Person newPerson = Person.createPerson(id, age, name, surname);
		
		if(!newPerson.equals(Person.NULL_PERSON)) {		
			cache.removePersonById(id);
			cache.addPerson(newPerson);
			return true;
		}
		return false;

	}
}
