package sandBox;

public class Node<T> {
	private Node<T> nextNode;
	private T elem;
	/**
	 * @param nextNode
	 * @param elem
	 */
	public Node(Node<T> nextNode, T elem) {
		super();
		this.nextNode = nextNode;
		this.elem = elem;
	}
	public T getElem() {
		return elem;
	}
	public void setElem(T elem) {
		this.elem = elem;
	}
	public Node<T> getNextNode() {
		return nextNode;
	}
	public void setNextNode(Node<T> nextNode) {
		this.nextNode = nextNode;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((elem == null) ? 0 : elem.hashCode());
		result = prime * result + ((nextNode == null) ? 0 : nextNode.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (elem == null) {
			if (other.elem != null)
				return false;
		} else if (!elem.equals(other.elem))
			return false;
		if (nextNode == null) {
			if (other.nextNode != null)
				return false;
		} else if (!nextNode.equals(other.nextNode))
			return false;
		return true;
	}
	
	
}
