package sandBox;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Streams {
	public static void main(String[] args) {

		List<String> names = new ArrayList<>();
		names.add("Alex");
		names.add("Amanda");
		names.add("Maksym");
		names.add("Maksym");
		names.add("Bran");
		names.add("Braian");

		Stream<String> streamNames = names.stream();
		streamNames.allMatch(name -> name.startsWith("A"));
		System.out.println(streamNames.filter(name -> name.startsWith("A"))
				.collect(Collectors.toList()));

		
		names.stream()
			.distinct()
			.map(String::length)
			.forEach(System.out::println);
		
		IntStream.of();
		
//		Streams streams = new Streams();
//		streams.getNonStaticCalculate();

	}

	public static Integer getLength(String str) {
		return Integer.valueOf(str.length());
	}

	public void getNonStaticCalculate() {
		List<String> names = new ArrayList<>();
		names.add("Alex");
		names.add("Amanda");
		names.add("Maksym");
		names.add("Maksym");
		names.add("Bran");
		names.add("Braian");

		names.stream()
			.distinct()
			.skip(3)
			.limit(2)
			.map(this::getLengthNonStatic)
			.forEach(System.out::println);

	}

	private Integer getLengthNonStatic(String str) {
		return Integer.valueOf(str.length());

	}
	
}

class ImutableObject{
	private final int counter;
	private final int value;
	
	
	/**
	 * @param counter
	 * @param value
	 */
	public ImutableObject(int counter, int value) {
		super();
		this.counter = counter;
		this.value = value;
	}
	
	
	public int getCounter() {
		return counter;
	}
	public ImutableObject setCounter(int counter) {
		return new ImutableObject(counter, this.value);
	}
	public int getValue() {
		return value;
	}
	public ImutableObject setValue(int value) {
		return new ImutableObject(this.counter, value);
	}
	
	
}
