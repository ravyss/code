package parserTask;

public class User {

	private long id;
	private long followersCount;
	private long favouritesCount;
	private String screenName;
	private String location;
	private String description;
	private String profileImageUrl;
	private String name;

	/**
	 * @param id
	 * @param followersCount
	 * @param favouritesCount
	 * @param screenName
	 * @param location
	 * @param description
	 * @param profileImageUrl
	 * @param name
	 */
	public User(long id, long followersCount, long favouritesCount, String screenName, String location,
			String description, String profileImageUrl, String name) {
		super();
		this.id = id;
		this.followersCount = followersCount;
		this.favouritesCount = favouritesCount;
		this.screenName = screenName;
		this.location = location;
		this.description = description;
		this.profileImageUrl = profileImageUrl;
		this.name = name;
		
		System.out.println("User's id is - " + id + "\n" + "He's name is - " + name + "\n" + "User's screen name is - "
				+ screenName + "\n" + "Description of user: " + description + "\n" + "Count of user's followers: "
				+ followersCount + "\n" + "Count of faworites: " + favouritesCount + "\n" + "User's location - "
				+ location + "\n" + "address of profile icon: " + profileImageUrl);
	}



	public String getName() {
		return name;
	}

	public long getId() {
		return id;
	}

	public long getFollowersCount() {
		return followersCount;
	}

	public long getFavouritesCount() {
		return favouritesCount;
	}

	public String getScreenName() {
		return screenName;
	}

	public String getLocation() {
		return location;
	}

	public String getDescription() {
		return description;
	}

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

}
