package stringTask;

class StringTask {
	public static void main(String[] args) {
		String adehio = "dhdieaohiae";
		String tetr = "who said the password is Petr";
		String palindrome = "Able was I ere I saw Elba";

		EditorString ed2 = new EditorString(adehio);
		ed2.polindromeProf();
		
		EditorString ed = new EditorString(tetr);
		ed.whoIsFirst();
		ed.deleteReturnSpaces();
		ed.firstHalfString();
		ed.lastHalfString();
		ed.multiplyString();
		ed.numberOfVowelConsonant();
		ed.queueVowelConsonant();
		ed.polindromeProf();
		ed.allCoincident(palindrome);

		EditorString ed1 = new EditorString(palindrome);
		ed1.whoIsFirst();
		ed1.deleteReturnSpaces();
		ed1.firstHalfString();
		ed1.lastHalfString();
		ed1.multiplyString();
		ed1.numberOfVowelConsonant();
		ed1.queueVowelConsonant();
		ed1.polindromeProf();
		ed1.allCoincident(tetr);

	}

}
