package genericTask.stuff;

import genericTask.Informational;

public abstract class HomeStuff implements Informational {
	private String name;
	private int weight;
	private String color;
	
	
	public HomeStuff(String name, String color, int weight) {
		this.name = name;
		this.color = color;
		this.weight = weight;		
	}
	@Override
	public String getInfo() {
    	return name + " " + weight + " " + color;
	}

}
