package genericTask;

import java.util.ArrayList;
import java.util.List;

import genericTask.stuff.Bumbox;
import genericTask.stuff.PC;
import genericTask.stuff.TV;

public class Box<T extends Informational>{
	
	private List<T> infoList = new ArrayList<>();
	
	boolean addInfoItem(T info) {
		if(infoList.size() == 10) {
			return false;
		}
		
		infoList.add(info);
		
		return true;
	}
	
	void showAllIformation(){
		for(int i = 0; i < infoList.size(); i++) {
			System.out.println(infoList.get(i).getInfo());
		}
	}
	
	public static void main(String[] args) {
		
		Box<Informational> boxInf = new Box<>();
		PC asus  = new PC("gr17","green", 2);
		TV samsung = new TV("newVision", "white", 4);
		Bumbox cort = new Bumbox("dolbit", "black", 1);
		
		boxInf.addInfoItem(asus);
		boxInf.addInfoItem(samsung);
		boxInf.addInfoItem(cort);
		
		boxInf.showAllIformation();
		
	}

}
