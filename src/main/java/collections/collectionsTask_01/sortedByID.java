package collections.collectionsTask_01;

import java.util.Comparator;

public class sortedByID implements Comparator<Person>{

	@Override
	public int compare(Person o1, Person o2) {
		long id1 = o1.getId();
        long id2 = o2.getId();

        if (id1 > id2) {
            return 1;
        } else if (id1 < id2) {
            return -1;
        } else {
            return 0;
        }
	}
}
