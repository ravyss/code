package collections.collectionsTask_05;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import collections.collectionsTask_01.Person;
import collections.collectionsTask_05.ChangeEvent.TypeChange;

public class PersonMgrTest {
	private PersonMgr meneger;
	static final long ID = 0;
	private PersonService mockService = mock(PersonServiceImpl.class);
//	private PersonChangeNotifier mockChange = mock(PersonMgr.class);
	private PersonChangeListener mockListener = mock(PersonChangeListenerImpl.class);
	private PersonChangeListener mockRemoveListener = mock(PersonRemoveListener.class);

	@Test
	public void AddListenerAndPerson_CorrectListenerAndPErson_AddEventOnceForEach() {
		Person afterChangePerson = Person.createPerson(ID, (short) 32, "AfterFirstName", "AfterSurname");
		ChangeEvent eventAdd = new ChangeEvent(Person.NULL_PERSON, afterChangePerson, TypeChange.Add);
		when(mockService.addPerson(afterChangePerson)).thenReturn(true);
		boolean isListenerAdded = meneger.addListener(mockListener);		
		
		boolean isPersonAdded = meneger.addPerson(afterChangePerson);

		assertTrue(isListenerAdded);
		assertTrue(isPersonAdded);

		verify(mockListener, times(1)).change(eventAdd);
		verify(mockService, times(1)).addPerson(afterChangePerson);
	}

	@Test
	public void RemoveListenerRemovePerson_FirstRemovedPersonCorrectRmovedListenerCorrectSecondRemovedPersonIncorrect_RemovedEventOnceForEach() {
		Person beforeChangePerson = Person.createPerson(ID, (short) 32, "AfterFirstName", "AfterSurname");
		ChangeEvent eventRemove = new ChangeEvent(beforeChangePerson, Person.NULL_PERSON,TypeChange.Remove);

		when(mockService.removePersonById(ID)).thenReturn(true);	
		when(mockService.getPersonById(ID)).thenReturn(beforeChangePerson);

		boolean isRemoveLIstenerAdded = meneger.addListener(mockRemoveListener);
		boolean isListenerRemoved = meneger.removeListener(mockRemoveListener);
		boolean isLIstenerAdded = meneger.addListener(mockListener);

		meneger.addPerson(beforeChangePerson);
		boolean isPersonRemoved = meneger.removePersonById(ID);
		
		when(mockService.removePersonById(ID)).thenReturn(false);
		boolean isPersonRemovedSecond = meneger.removePersonById(ID);		

		assertTrue(isRemoveLIstenerAdded);
		assertTrue(isLIstenerAdded);
		assertTrue(isPersonRemoved);
		assertFalse(isPersonRemovedSecond);
		assertTrue(isListenerRemoved);

		verify(mockRemoveListener, times(0)).change(eventRemove);
		verify(mockListener, times(1)).change(eventRemove);
		verify(mockService, times(1)).addPerson(beforeChangePerson);
		verify(mockService, times(2)).removePersonById(ID);
	}

	@Test
	public void UpdatePerson_Correct_ChangedOnce() {
		Person afterChangePerson = Person.createPerson(ID, (short) 23, "AfterFirstName", "BeforeSurname");		
		ChangeEvent eventUpdate = new ChangeEvent(afterChangePerson, afterChangePerson, TypeChange.Update);

		when(mockService.changeName(ID, afterChangePerson.getFirstName())).thenReturn(true);
		when(mockService.addPerson(afterChangePerson)).thenReturn(true);
		when(mockService.getPersonById(ID)).thenReturn(afterChangePerson);
		meneger.addListener(mockListener);		
		
		boolean isChanged  = meneger.changeName(ID, afterChangePerson.getFirstName());

		assertTrue(isChanged);		
		verify(mockListener, times(1)).change(eventUpdate);
	}

	@Test
	public void AddPerson_NullPerson_AddEventDontTrigers() {
		Person afterChangePerson = Person.createPerson(ID, (short) 32, null, "AfterSurname");
		ChangeEvent eventAdd = new ChangeEvent(Person.NULL_PERSON, afterChangePerson, TypeChange.Add);
		when(mockService.addPerson(afterChangePerson)).thenReturn(false);
		boolean isListenerAdded = meneger.addListener(mockListener);
		
		boolean isPersonAdded = meneger.addPerson(afterChangePerson);

		assertTrue(isListenerAdded);
		assertFalse(isPersonAdded);

		verify(mockListener, times(0)).change(eventAdd);
		verify(mockService, times(1)).addPerson(afterChangePerson);
	}

	@Test
	public void UpdatePersonWithRealService_Correct_TriggerChangeEvent() {
		Person beforeChangePerson = Person.createPerson(ID, (short) 23, "BeforeFirstName", "BeforeSurname");	
		Person afterChangePerson = Person.createPerson(ID, (short) 32, "AfterFirstName", "AfterSurname");			
		PersonCache cache = new PersonCache();
		PersonService personService = new PersonServiceImpl(cache);
		meneger = new PersonMgr(personService);		
		boolean isPersonAdded = meneger.addPerson(beforeChangePerson);
		
		boolean isPersonChangeName = meneger.changeName(ID, afterChangePerson.getFirstName());
		boolean isPersonChangeSurname = meneger.changeSurname(ID, afterChangePerson.getSurname());
		boolean isPersonChangeAge = meneger.changeAge(ID, afterChangePerson.getAge());
		
		assertTrue(isPersonAdded);
		assertTrue(isPersonChangeName);
		assertTrue(isPersonChangeSurname);
		assertTrue(isPersonChangeAge);		
		assertSame(meneger.getPersonById(ID).getFirstName(), afterChangePerson.getFirstName());	
		assertSame(meneger.getPersonById(ID).getSurname(), afterChangePerson.getSurname());	
		assertSame(meneger.getPersonById(ID).getAge(), afterChangePerson.getAge());	
	}
	
	@Test
	public void UpdatePersonWithRealService_Null_TriggerChangeEvent() {
		Person beforeChangePerson = Person.createPerson(ID, (short) 23, "BeforeFirstName", "BeforeSurname");		
		PersonCache cache = new PersonCache();
		PersonService personService = new PersonServiceImpl(cache);
		meneger = new PersonMgr(personService);		
		boolean isPersonAdded = meneger.addPerson(beforeChangePerson);
		
		boolean isPersonChangeName = meneger.changeName(ID, Person.NULL_PERSON.getFirstName());
		boolean isPersonChangeSurname = meneger.changeSurname(ID, Person.NULL_PERSON.getSurname());
		boolean isPersonChangeAge = meneger.changeAge(ID, Person.NULL_PERSON.getAge());
		
		assertTrue(isPersonAdded);
		assertTrue(isPersonChangeName);
		assertTrue(isPersonChangeSurname);
		assertFalse(isPersonChangeAge);		
		assertNotNull(meneger.getPersonById(ID).getFirstName());
		assertNotNull(meneger.getPersonById(ID).getSurname());		
		assertSame(meneger.getPersonById(ID).getFirstName(), Person.NULL_PERSON.getFirstName());	
		assertSame(meneger.getPersonById(ID).getSurname(), Person.NULL_PERSON.getSurname());
	}
	
	
	@Before
	public void before() {
		meneger = new PersonMgr(mockService);
	}

	@After
	public void after() {
		meneger = null;
		mockService = mock(PersonServiceImpl.class);
		mockListener = mock(PersonChangeListenerImpl.class);
		mockRemoveListener = mock(PersonRemoveListener.class);
	}
}
